<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('dashboard');
})->name('/');

Route::get('/table', function () {
    return view('halaman.table');
})->name('table');

Route::get('/data-table', function () {
    return view('halaman.data_table');
})->name('data-table');


// CRUD cast
Route::get('cast', 'CastController@index')->name('cast');
Route::get('cast/create', 'CastController@create')->name('cast.create');
Route::post('cast', 'CastController@store')->name('cast.store');
Route::get('cast/{cast_id}', 'CastController@show');
Route::get('cast/{cast_id}/edit', 'CastController@edit');
Route::put('cast/{cast_id}', 'CastController@update');
Route::delete('cast/{cast_id}', 'CastController@destroy');