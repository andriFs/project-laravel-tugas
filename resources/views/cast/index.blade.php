@extends('layout.app')

@section('content-header')
    <section class="content-header">
        <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            <h1>Halaman Data Cast</h1>
            </div>
            <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{ route('/') }}">Dashboard</a></li>
                <li class="breadcrumb-item active">Cast</li>
            </ol>
            </div>
        </div>
        </div><!-- /.container-fluid -->
    </section>
@endsection

@section('main-content')
    <section class="content">
        <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                    <h3 class="card-title">Table Cast</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                        <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                        <i class="fas fa-times"></i>
                        </button>
                    </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <a href="{{ route('cast.create') }}" class="btn btn-primary mb-2">Tambah Cast</a>
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th scope="col" style="width: 5%" class="text-center">#</th>
                                <th scope="col" style="width: 30%" class="text-center">Nama</th>
                                <th scope="col" style="width: 10%" class="text-center">Umur</th>
                                <th scope="col" style="width: 35%" class="text-center">Bio</th>
                                <th scope="col" style="width: 20%" class="text-center">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                @forelse ( $cast as $key => $data )
                                    <tr>
                                        <td class="text-center">{{ $key + 1 }}</td>
                                        <td>{{ $data->nama }}</td>
                                        <td class="text-center">{{ $data->umur }}</td>
                                        <td>{{substr($data->bio, 0, 30)}} ...</td>
                                        <td class="text-center">
                                            <form action="/cast/{{$data->id}}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <a href="/cast/{{ $data->id }}" class="btn btn-info btn-sm">Detail</a>
                                                <a href="/cast/{{ $data->id }}/edit" class="btn btn-primary btn-sm">Edit</a>
                                                <input type="submit" class="btn btn-danger my-1 btn-sm" value="Delete">
                                            </form>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="5" class="text-center text-danger">Data Kosong !</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
            <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
@endsection
